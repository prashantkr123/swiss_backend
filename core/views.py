from django.http import JsonResponse
from django.contrib.auth import authenticate
from .models import UserProfile
import jwt
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import json

SECRET_KEY = settings.SECRET_KEY


def login_view(request):
    req_data = json.loads(request.body.decode('utf-8'))
    username = req_data['username']
    password = req_data['password']
    user = authenticate(username=username, password=password)
    if user is None:
        return JsonResponse(
            {"error": "The request has not been applied" +
             " because it lacks valid authentication credentials " +
             "for the target resource."},
            status=401)
    else:
        token = jwt.encode({'user_id': user.id}, SECRET_KEY)
        return JsonResponse({"success": "true", "token":
                            token.decode("utf-8")})


def signup_view(request):
    req_data = json.loads(request.body.decode('utf-8'))
    username = req_data['username']
    password = req_data['password']
    first_name = req_data['first_name']
    last_name = req_data['last_name']
    country_code = req_data['country_code']
    if (len(password) < 8):
        return JsonResponse({"failure": "Need Minimum of 8 characters"},
                            status=400)
    try:
        user = User.objects.get(username=username)
        return JsonResponse({"failure": "The request could not be completed " +
                             "due to a conflict with the current state " +
                             "of the target resource. This code is used in " +
                             "situations where the user might be able to " +
                             "resolve the conflict and resubmit the " +
                             "request"}, status=409)
    except ObjectDoesNotExist:
        user = User.objects.create_user(username=username,
                                        password=password,
                                        first_name=first_name,
                                        last_name=last_name)
        user_profile = UserProfile(user=user, country_code=country_code)
        user_profile.save()
    return JsonResponse({"success": "The request has been fulfilled and " +
                         "resulted in a new resource being created"},
                        status=201)
