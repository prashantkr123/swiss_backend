"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from core import views as core_views
from tournament import views as tour_views

urlpatterns = [
    url(r'^api/signup/$', core_views.signup_view),
    url(r'^api/login/$', core_views.login_view),
    url(r'^api/tournaments/$', tour_views.add_tournament),
    url(r'^api/tournaments/(?P<tournament_id>[0-9]+)/$',
        tour_views.update_status),
    url(r'^api/players/$', tour_views.add_player),
    url(r'^api/matches/$', tour_views.report_match),
    url(r'^api/standings/$',
        tour_views.player_standings),
    url(r'^api/swiss_pairings/$', tour_views.swiss_pairings),
    url(r'^api/matchvalid/(?P<tournament_id>[0-9]+)/$',
        tour_views.matchvalid),
    url(r'^api/tournament/(?P<tournament_id>[0-9]+)/'
        'status/(?P<current_round>[0-9]+)/data/$',
        tour_views.round_data),
    url(r'^api/reset_db/$', tour_views.reset_db),
    url(r'^admin/', admin.site.urls),
]
