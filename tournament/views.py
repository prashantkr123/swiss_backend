from core.decorators import token_required
from .models import Tournament, TournamentPlayer, Match, Player
from django.contrib.auth.models import User
from core.models import UserProfile
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count
from collections import OrderedDict
import json
import math
# Create your views here.


@token_required
def reset_db(request):
    MODELS = [Tournament, Player, TournamentPlayer, Match, User, UserProfile]
    for Model in MODELS:
        Model.objects.all().delete()
    return JsonResponse({"ok": "ok"})


@token_required
def add_tournament(request):
    if request.method == 'POST':
        req_body = json.loads(request.body.decode('utf-8'))
        tournament_name = req_body['tournament']
        try:
            Tournament.objects.get(name=tournament_name,
                                   creator=request.swiss_user)
            return JsonResponse({"status": "Tournament already exists"},
                                status=409)
        except ObjectDoesNotExist:
            tournament = Tournament.objects.create(name=tournament_name,
                                                   creator=request.swiss_user)
            return JsonResponse({'id': tournament.id,
                                 'name': tournament.name,
                                 'status': tournament.status})
    elif request.method == 'GET':
        tournaments = []
        tournament_objs = request.swiss_user.tournament_set.all()
        for tournament in tournament_objs:
            dictionary = {}
            dictionary['name'] = tournament.name
            dictionary['id'] = tournament.id
            dictionary['status'] = tournament.status
            tournaments.append(dictionary)

        return JsonResponse(tournaments, safe=False)


@token_required
def create_tournament(request):
    req_body = json.loads(request.body.decode('utf-8'))
    tournament_name = req_body['tournament_name']
    try:
        Tournament.objects.get(name=tournament_name)
        return JsonResponse({"status": "Tournament already exists"},
                            status=400)
    except ObjectDoesNotExist:
        tournament = Tournament.objects.create(
            name=tournament_name,
            creator=request.swiss_user)
        return JsonResponse({'id': tournament.id, 'name':
                            tournament.name,
                            'status': tournament.status}, status=201)


@token_required
def add_player(request):
    print('Entry point for add_player')
    if request.method == 'GET':
        player_dict = {}
        tournament_id = request.GET.get('tournament_id')
        tournament = Tournament.objects.get(id=tournament_id)
        players_list = Player.objects.filter(creator=request.swiss_user)
        player_list = list(players_list)
        tournament_players = tournament.players.all()
        player_list = []
        for player in tournament_players:
            temp = {}
            temp['id'] = player.player.id
            temp['name'] = player.player.name
            player_list.append(temp)
        return JsonResponse(player_list, safe=False)
    if request.method == 'POST':
        req_body = json.loads(request.body.decode('utf-8'))
        tournament_id = req_body['tournament_id']
        player_name = req_body['player']
        player_dict = {}
        tournament = Tournament.objects.get(id=tournament_id)
        if(tournament.status == 3):
            return JsonResponse({'error': 'CANNOT ADD PLAYER IN THE' +
                                 'FINISHED TOURNAMENT'}, status=400)
        elif (tournament.status == 2):
            return JsonResponse({'error': 'CANNOT ADD PLAYER IN THE' +
                                 'RUNNING TOURNAMENT'}, status=401)
        else:
            try:
                player = Player.objects.get(name=player_name,
                                            creator=request.swiss_user)
                try:
                    TournamentPlayer.objects.get(
                        player=player, tournament=tournament)
                    return JsonResponse({"Error": 'Player is already'
                                        'available in this tournament'},
                                        status=409)
                except ObjectDoesNotExist:
                    TournamentPlayer.objects.create(player=player,
                                                    tournament=tournament)
            except ObjectDoesNotExist:
                player = Player.objects.create(name=player_name,
                                               creator=request.swiss_user)
                TournamentPlayer.objects.create(player=player,
                                                tournament=tournament)
        players = tournament.players.all()
        for pl in players:
            player_dict[pl.player.id] = pl.player.name
        return JsonResponse({"id": player.id, "name": player.name})


@token_required
def update_status(request, tournament_id):
    tournament = Tournament.objects.get(id=tournament_id)
    players_obj = TournamentPlayer.objects.filter(
        tournament=tournament_id).values()
    matches_obj = Match.objects.filter(tournament=tournament_id).values()
    num_players = len(players_obj)
    num_matches = len(matches_obj)
    if (num_players != 0 and ((num_players & (num_players - 1)) == 0)):
        tournament.status = 2
    elif num_matches == (math.log(num_players) * (num_players / 2)):
        tournament.status = 3
    tournament.save()
    return JsonResponse({"status": "updated"}, status=200)


def report_match(tournament, player1, player2, winner, current_round):
    try:
        Match.objects.get(tournament=tournament,
                          round=current_round,
                          player_one=player1,
                          player_two=player2)
    except ObjectDoesNotExist:
        Match.objects.create(tournament=tournament,
                             round=current_round,
                             player_one=player1,
                             player_two=player2,
                             status=2,
                             winner=winner)


@token_required
def player_standings(request):
    tournament_id = request.GET.get('tournament_id')
    tournament = Tournament.objects.get(id=tournament_id)
    current_round = int(tournament.current_round) - 1
    player_stats = player_standings1(tournament_id, current_round)
    round_no = int(math.log(len(player_stats), 2))
    rounds = []
    for i in range(0, int(round_no)):
        rounds.append(i + 1)
    return JsonResponse({'player_stats': player_stats, 'rounds': rounds,
                         'current_round': current_round})


def player_standings1(tournament_id, round_num):
    tournament = Tournament.objects.get(id=tournament_id)
    all_players = TournamentPlayer.objects.filter(
        tournament=tournament).values('player')
    winners = []
    player_stats = []
    all_player = []
    for i in all_players:
        all_player.append(i['player'])
    qs = Match.objects.filter(tournament=tournament, round__lte=round_num
                              ).values('winner').annotate(wins=Count('winner')
                                                          ).order_by('-wins')
    for i in qs:
        d = OrderedDict()
        d['player_id'] = i['winner']
        d['player_name'] = Player.objects.get(id=i['winner']).name
        d['wins'] = i['wins']
        d['lost'] = int(round_num) - int(i['wins'])
        winners.append(i['winner'])
        player_stats.append(d)
    for i in all_player:
        if i not in winners:
                a = OrderedDict()
                a['player_id'] = i
                a['player_name'] = Player.objects.get(id=i).name
                a['wins'] = 0
                a['lost'] = round_num
                player_stats.append(a)
    return player_stats


def played_before(player1, player2):
    player1 = Player.objects.get(id=player1)
    player2 = Player.objects.get(id=player2)
    try:
        Match.objects.get(player_one=player1, player_two=player2)
    except ObjectDoesNotExist:
        try:
            Match.objects.get(player_one=player2, player_two=player1)
        except ObjectDoesNotExist:
            return False
    return True


def swiss_pairings(request):
    tournament_id = request.GET.get('tournament_id')
    round_num = request.GET.get('round_num')
    r = player_standings1(tournament_id, round_num)
    player_stats = r
    swiss_pairs = []
    while len(player_stats) > 0:
        player1 = player_stats[0]
        index_player2 = 1
        while index_player2 < len(player_stats):
            player2 = player_stats[index_player2]
            played_already = played_before(player1['player_id'],
                                           player2['player_id'])
            if played_already is False:
                temp_list = []
                temp_list.append(player1['player_id'])
                temp_list.append(player1['player_name'])
                temp_list.append(player2['player_id'])
                temp_list.append(player2['player_name'])
                swiss_pairs.append(temp_list)
                player_stats.pop(0)
                player_stats.pop(index_player2 - 1)
                break
            else:
                index_player2 += 1
    print(swiss_pairs)
    return JsonResponse(swiss_pairs, safe=False)


@token_required
def matchvalid(request, tournament_id):
    tournament = Tournament.objects.get(id=tournament_id,
                                        creator=request.swiss_user)
    status = tournament.status
    if status in [1, 2]:
        num_players = TournamentPlayer.objects.filter(
            tournament=tournament).count()
        flag = (num_players != 0) and (num_players != 1) and (
               (num_players & (num_players - 1)) == 0)
        if flag:
            tournament.status = 2
            tournament.save()
            return JsonResponse({'success': 'MATCH STARTED'}, status=200)
        else:
            return JsonResponse({'error': 'MATCH CAN\'T STARTED'}, status=400)
    else:
        return JsonResponse({'error': 'TOURNAMENT GETS OVER DUDE!!!'},
                            status=401)


@token_required
def round_data(request, tournament_id, current_round):
    req_body = json.loads(request.body.decode('utf-8'))
    player_one_list = req_body['player_one']
    player_two_list = req_body['player_two']
    winner_list = req_body['winner']
    match_round = int(current_round) + 1
    tournament = Tournament.objects.get(id=tournament_id)
    players_obj = TournamentPlayer.objects.filter(
        tournament=tournament_id).values()
    if tournament.status == 1 or tournament.status == 2:
            for i in range(0, len(player_one_list)):
                player_one = Player.objects.get(creator=request.swiss_user,
                                                name=player_one_list[i])
                player_two = Player.objects.get(creator=request.swiss_user,
                                                name=player_two_list[i])
                player_win = Player.objects.get(creator=request.swiss_user,
                                                name=winner_list[i])
                report_match(tournament, player_one, player_two, player_win,
                             match_round)
            matches_obj = Match.objects.filter(
                tournament=tournament_id).values()
            num_players = len(players_obj)
            num_matches = len(matches_obj)
            tournament.current_round += 1
            print(num_matches == int(math.log(num_players, 2)) * (num_players // 2))
            if (num_matches == int(math.log(num_players, 2)) * (num_players // 2)):
                print('hereeeeeeeeeeee')
                tournament.status = 3
    tournament.save()
    return JsonResponse({'succes': 'match is started now'}, status=200)
