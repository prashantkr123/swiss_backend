import unittest
import requests
import json


BASE_URL = 'http://localhost:8888'


class BaseSwissTest(unittest.TestCase):

    def setUp(self):

        auth = {
            'username': 'New',
            'password': 'qwerty123',
            'first_name': 'Nicholas',
            'last_name': 'Holland',
            'country_code': '12'
        }
        r = requests.post(
            "{}/api/signup/".format(BASE_URL),
            json=auth
        )
        auth = {
            'username': 'New',
            'password': 'qwerty123'
        }
        r = requests.post(
            "{}/api/login/".format(BASE_URL),
            json=auth
        )
        data = self.resp_to_json(r)
        self.token = data['token']

    def resp_to_json(self, r):
        return json.loads(r.content.decode('utf-8'))

    def tearDown(self):
        requests.post(
            "{}/api/reset_db/".format(BASE_URL))


class CreateTournamentTest(BaseSwissTest):
    def test_create_tournament(self):
        payload = {'tournament_name': 'Wimbledon9'}
        r = requests.post("{}/api/tournaments/".format(BASE_URL), headers={
            'X-Auth-Token': self.token
        }, json=payload)
        self.assertEqual(r.status_code, 201)
        content = self.resp_to_json(r)
        self.assertEqual(content['name'],
                         payload['tournament_name'])
        payload = {'tournament_name': 'Wimbledon9',
                   'player_name': 'Ernst Williamsfre'}
        r = requests.post("{}/api/players/".format(BASE_URL),
                          headers={'X-Auth-Token': self.token}, json=payload)
        self.assertEqual(r.status_code, 201)


''' def test_signup_view(self):
               payload = {'username': 'Bond', 'first_name': 'James',
                   'last_name': 'Bond', 'password': 'qwert123',
                   'country_code': 'US'}
            r = requests.post("{}/api/signup/".format(BASE_URL),
                          headers={'X-Auth-Token': self.token}, json=payload)
            self.assertEqual(r.status_code, 201)

    def test_report_match(self):
        payload = {'tournament_id': '7', 'player1_id': '1',
                   'player2_id': '2', 'winner_id': '1', 'round': '1'}
        r = requests.post("{}/api/tournaments/".format(BASE_URL), headers={
            'X-Auth-Token': self.token
        }, json=payload)
        self.assertEqual(r.status_code, 201)

    def test_player_standings(self):
        payload = {'tournament_id': '9'}
        r = requests.post("{}/api/player_standings/".format(BASE_URL),
                          headers={
            'X-Auth-Token': self.token
        }, json=payload)
        self.assertEqual(r.status_code, 201)

    def test_swiss_pairing(self):
        payload = {'tournament_id': '9'}
        r = requests.post("{}/api/swiss_pairing/".format(BASE_URL), headers={
            'X-Auth-Token': self.token
        }, json=payload)
        self.assertEqual(r.status_code, 201)
    '''

if __name__ == '__main__':
    unittest.main()
