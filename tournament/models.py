from django.db import models
from django.contrib.auth.models import User


TOURNAMENT_STATUS_CHOICES = (
    (1, 'NOT STARTED'),
    (2, 'STARTED'),
    (3, 'COMPLETED')
)


class Tournament(models.Model):
    name = models.CharField(max_length=100)
    creator = models.ForeignKey(User)
    status = models.IntegerField(choices=TOURNAMENT_STATUS_CHOICES, default=1)
    current_round = models.IntegerField(default=1)

    def __str__(self):
        return '{} - {} - Status: {}'.format(
            self.name,
            self.creator,
            self.status
        )

    class Meta:
        unique_together = ('creator', 'name')


class Player(models.Model):
    name = models.CharField(max_length=50)
    creator = models.ForeignKey(User)

    class Meta:
        unique_together = ('name', 'creator')

    def __str__(self):
        return '{} - {}'.format(self.name, self.creator)


class TournamentPlayer(models.Model):
    tournament = models.ForeignKey(Tournament, related_name='players')
    player = models.ForeignKey(Player)

    class Meta:
        unique_together = ('tournament', 'player')


MATCH_STATUS_CHOICES = (
    (1, 'UPCOMING'),
    (2, 'CONDUCTED')
)


class Match(models.Model):
    tournament = models.ForeignKey(Tournament)
    status = models.IntegerField(choices=MATCH_STATUS_CHOICES, default=1)
    round = models.IntegerField()
    player_one = models.ForeignKey(Player, related_name='player_one')
    player_two = models.ForeignKey(Player, related_name='player_two')
    winner = models.ForeignKey(Player, related_name='winner')

    def __str__(self):
        return '{} {}'.format(
            self.tournament,
            self.round
        )
